<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaftarUmkmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daftar_umkm', function (Blueprint $table) {
            $table->id();
            $table->string('nib')->unique();
            $table->string('nama_pemilik');
            $table->string('nik');
            $table->string('iumk')->nullable();
            $table->date('tgl_terbit');
            $table->string('kekayaan');
            $table->string('nama_usaha');
            $table->enum('sektor', ['Fashion', 'Teknologi', 'Kuliner', 'Cinderamata', 'Kosmetik', 'Agro Bisnis', 'Otomotif']);
            $table->string('kbli')->nullable();
            $table->enum('kegiatan_usaha', ['Fashion', 'Teknologi', 'Kuliner', 'Cinderamata', 'Kosmetik', 'Agro Bisnis', 'Otomotif']);
            $table->text('alamat_usaha')->nullable();
            $table->string('telp_pemilik')->nullable();
            $table->string('modal');
            $table->string('hasil');
            $table->integer('jml_tenaga_kerja');
            $table->string('npwp')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daftar_umkm');
    }
}
