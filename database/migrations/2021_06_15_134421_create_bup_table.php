<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bup', function (Blueprint $table) {
            $table->id('id_bup');
            $table->string('nib');
            $table->string('nama_pemilik');
            $table->string('nik');
            $table->string('iumk')->nullable();
            $table->text('alamat_usaha')->nullable();
            $table->string('telp_pemilik')->nullable();
            $table->string('npwp')->nullable();
            $table->string('no_rek');
            $table->string('ektp');
            $table->string('kk_pemilik');
            $table->string('ket_usaha');
            $table->string('buku_tabungan');
            $table->string('doc_pernyataan');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bup');
    }
}
