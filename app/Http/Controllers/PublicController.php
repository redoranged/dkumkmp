<?php

namespace App\Http\Controllers;

use App\Models\Bup;
use App\Models\DaftarUmkm;
use App\Models\JadwalPameran;
use App\Models\PengumumanBup;
use App\Models\ProdukUmkm;
use App\Models\ProdukUnggulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class PublicController extends Controller
{
    /**
     * Show the beranda.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function beranda()
    {
        return view('public.beranda');
    }

    /**
     * Show the daftarUmkm.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function daftarUmkm(Request $request)
    {
        if($request->has('search'))
        {
            $daftarUmkm = DaftarUmkm::where('nib', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->orWhere('nama_pemilik', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->orWhere('nama_usaha', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->orWhere('sektor', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->orWhere('kegiatan_usaha', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->orWhere('alamat_usaha', 'like', '%'.$request->search.'%')
                            ->whereNotNull('iumk')
                            ->paginate(10)->withQueryString();
        }else{
            $daftarUmkm = DaftarUmkm::paginate(10);
        }
        return view('public.daftarUmkm.index', compact('daftarUmkm'));
    }

    /**
     * Show the produkUmkm.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function produkUmkm(Request $request)
    {
        if($request->has('search'))
        {
            $search = $request->search;
            $produkUmkm = ProdukUmkm::where('bpom', 'like', '%'.$request->search.'%')
                            ->orWhere('nama_produk', 'like', '%'.$request->search.'%')
                            ->orWhereHas('daftarUmkm', function ($query) use ($search){
                                $query->where('nib', 'like', '%'.$search.'%')
                                    ->orWhere('nama_usaha', 'like', '%'.$search.'%');
                            })
                            ->paginate(10)->withQueryString();
        }else{
            $produkUmkm = ProdukUmkm::paginate(10);
        }
        return view('public.produkUmkm.index', compact('produkUmkm'));
    }

    /**
     * Show the produkUnggulan.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function produkUnggulan(Request $request)
    {
        if($request->has('search'))
        {
            $search = $request->search;
            $produkUnggulan = ProdukUnggulan::where('bpom', 'like', '%'.$request->search.'%')
                                ->orWhereHas('produkUmkm', function ($query) use ($search){
                                    $query->where('nama_produk', 'like', '%'.$search.'%');
                                })
                                ->paginate(10)->withQueryString();
        }else{
            $produkUnggulan = ProdukUnggulan::paginate(10);
        }
        return view('public.produkUnggulan.index', compact('produkUnggulan'));
    }

    /**
     * Show the jadwalPameran.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function jadwalPameran(Request $request)
    {
        if($request->has('search'))
        {
            $jadwalPameran = JadwalPameran::where('nama_pameran', 'like', '%'.$request->search.'%')
                            ->orWhere('lokasi_pameran', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $jadwalPameran = JadwalPameran::paginate(10);
        }
        return view('public.jadwalPameran.index', compact('jadwalPameran'));
    }

    /**
     * Show the bup.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bup(Request $request)
    {
        if($request->has('search'))
        {
            $bup = PengumumanBup::where('judul_pengumuman', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $bup = PengumumanBup::paginate(10);
        }
        return view('public.bup.index', compact('bup'));
    }

    /**
     * Downloading Doc Pernyataan
     * 
     * @param  \App\Models\PengumumanBup  $bup
     * @return \Illuminate\Http\Response
     */
    public function bupDownload(PengumumanBup $pengumumanBup)
    {
        $file= public_path(). Storage::url($pengumumanBup->doc_pengumuman);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'Pengumuman_'.$pengumumanBup->judul_pengumuman.'.pdf', $headers);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function bupCreate()
    {
        return view('public.bup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bupStore(Request $request)
    {
        $request->validate([
            'nib' => ['required', 'string', 'max:255', 'unique:bup'],
            'nama_pemilik' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'string', 'min:16', 'max:255'],
            'no_rek' => ['required'],
            'doc_pernyataan' => ['required'],
        ]);

        $path = $request->file('doc_pernyataan')->storeAs(
            'pernyataan' , $request->nib, 'public'
        );

        $store = $request->except(['doc_pernyataan']);

        $store['doc_pernyataan'] = $path;

        $bup = Bup::create($store);
        
        return redirect()->route('public.bup')->with('success', 'Berhasil mengajukan BUP <strong>'.$bup->nib.'</strong>.');
    }
}
