<?php

namespace App\Http\Controllers;

use App\Models\PengumumanBup;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class PengumumanBupController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $pengumumanBup = PengumumanBup::where('judul_pengumuman', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $pengumumanBup = PengumumanBup::paginate(10);
        }
        return view('admin.pengumumanBup.index', compact('pengumumanBup'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pengumumanBup.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'judul_pengumuman' => ['required', 'string', 'max:255'],
            'doc_pengumuman' => ['required'],
        ]);

        $path = $request->file('doc_pengumuman')->storeAs(
            'pengumuman' , $request->judul_pengumuman, 'public'
        );

        $store = $request->except(['doc_pengumuman']);

        $store['doc_pengumuman'] = $path;

        $pengumumanBup = PengumumanBup::create($store);
        
        return redirect()->route('admin.pengumumanBup.show', $pengumumanBup)->with('success', 'Berhasil menambah Pengumuman BUP <strong>'.$pengumumanBup->judul_pengumuman.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PengumumanBup  $pengumumanBup
     * @return \Illuminate\Http\Response
     */
    public function show(PengumumanBup $pengumumanBup)
    {
        return view('admin.pengumumanBup.show', compact('pengumumanBup'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PengumumanBup  $pengumumanBup
     * @return \Illuminate\Http\Response
     */
    public function edit(PengumumanBup $pengumumanBup)
    {
        return view('admin.pengumumanBup.edit', compact('pengumumanBup'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PengumumanBup  $pengumumanBup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PengumumanBup $pengumumanBup)
    {
        $request->validate([
            'judul_pengumuman' => ['required', 'string', 'max:255'],
        ]);
        
        if($request->has('doc_pengumuman')){
            Storage::disk('public')->delete($pengumumanBup->doc_pengumuman);
            $path = $request->file('doc_pengumuman')->storeAs(
                'pengumuman' , $request->judul_pengumuman, 'public'
            );
            $update = $request->except(['doc_pengumuman']);
            $update['doc_pengumuman'] = $path;
        }else{
            $update = $request->all();
        }

        $pengumumanBup->update($update);
        
        return redirect()->route('admin.pengumumanBup.show', $pengumumanBup)->with('success', 'Berhasil mengubah Pengumuman BUP <strong>'.$pengumumanBup->judul_pengumuman.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PengumumanBup  $pengumumanBup
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengumumanBup $pengumumanBup)
    {
        $name = $pengumumanBup->judul_pengumuman;
        Storage::disk('public')->delete($pengumumanBup->doc_pengumuman);
        $pengumumanBup->delete();

        return redirect()->route('admin.pengumumanBup.index')->with('success', 'Berhasil menghapus Pengumuman BUP <strong>'.$name.'</strong>.');
    }

    /**
     * Downloading Doc Pengumuman
     * 
     * @param  \App\Models\PengumumanBup  $bup
     * @return \Illuminate\Http\Response
     */
    public function download(PengumumanBup $pengumumanBup)
    {
        $file= public_path(). Storage::url($pengumumanBup->doc_pengumuman);

        $headers = array(
                'Content-Type: application/pdf',
                );

        return response()->download($file, 'Pengumuman_'.$pengumumanBup->judul_pengumuman.'.pdf', $headers);
    }
}
