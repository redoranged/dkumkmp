<?php

namespace App\Http\Controllers;

use App\Models\JadwalPameran;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class JadwalPameranController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $jadwalPameran = JadwalPameran::where('nama_pameran', 'like', '%'.$request->search.'%')
                            ->orWhere('lokasi_pameran', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $jadwalPameran = JadwalPameran::paginate(10);
        }
        return view('admin.jadwalPameran.index', compact('jadwalPameran'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hak_akses != 'staf_dpmpt'){
            return view('admin.jadwalPameran.create');
        }else{
            return redirect()->route('admin.jadwalPameran.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama_pameran' => ['required', 'string', 'max:255'],
            'poster_pameran' => ['required'],
            'tgl_pameran' => ['required', 'date'],
            'lokasi_pameran' => ['required'],
        ]);

        $path = $request->file('poster_pameran')->storeAs(
            'pameran' , $request->nama_pameran.'_'.$request->tgl_pameran, 'public'
        );

        $store = $request->except(['poster_pameran']);

        $store['poster_pameran'] = $path;

        $jadwalPameran = JadwalPameran::create($store);
        
        return redirect()->route('admin.jadwalPameran.show', $jadwalPameran)->with('success', 'Berhasil menambah Jadwal Pameran <strong>'.$jadwalPameran->nama_pameran.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JadwalPameran  $jadwalPameran
     * @return \Illuminate\Http\Response
     */
    public function show(JadwalPameran $jadwalPameran)
    {
        return view('admin.jadwalPameran.show', compact('jadwalPameran'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JadwalPameran  $jadwalPameran
     * @return \Illuminate\Http\Response
     */
    public function edit(JadwalPameran $jadwalPameran)
    {
        if(Auth::user()->hak_akses != 'staf_dpmpt'){
            return view('admin.jadwalPameran.edit', compact('jadwalPameran'));
        }else{
            return redirect()->route('admin.jadwalPameran.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JadwalPameran  $jadwalPameran
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JadwalPameran $jadwalPameran)
    {
        $request->validate([
            'nama_pameran' => ['required', 'string', 'max:255'],
            'tgl_pameran' => ['required', 'date'],
            'lokasi_pameran' => ['required'],
        ]);
        
        if($request->has('poster_pameran')){
            Storage::disk('public')->delete($jadwalPameran->poster_pameran);
            $path = $request->file('poster_pameran')->storeAs(
                'pameran' , $request->nama_pameran.'_'.$request->tgl_pameran, 'public'
            );
            $update = $request->except(['poster_pameran']);
            $update['poster_pameran'] = $path;
        }else{
            $update = $request->all();
        }

        $jadwalPameran->update($update);
        
        return redirect()->route('admin.jadwalPameran.show', $jadwalPameran)->with('success', 'Berhasil mengubah Jadwal Pameran <strong>'.$jadwalPameran->nama_pameran.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JadwalPameran  $jadwalPameran
     * @return \Illuminate\Http\Response
     */
    public function destroy(JadwalPameran $jadwalPameran)
    {
        if(Auth::user()->hak_akses != 'staf_dpmpt'){
            $name = $jadwalPameran->nama_pameran;
            Storage::disk('public')->delete($jadwalPameran->poster_pameran);
            $jadwalPameran->delete();
    
            return redirect()->route('admin.jadwalPameran.index')->with('success', 'Berhasil menghapus Jadwal Pameran <strong>'.$name.'</strong>.');
        }else{
            return redirect()->route('admin.jadwalPameran.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }
}
