<?php

namespace App\Http\Controllers;

use App\Models\DaftarUmkm;
use App\Models\ProdukUmkm;
use App\Models\ProdukUnggulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class ProdukUmkmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $search = $request->search;
            $produkUmkm = ProdukUmkm::where('bpom', 'like', '%'.$request->search.'%')
                            ->orWhere('nama_produk', 'like', '%'.$request->search.'%')
                            ->orWhereHas('daftarUmkm', function ($query) use ($search){
                                $query->where('nib', 'like', '%'.$search.'%')
                                    ->orWhere('nama_usaha', 'like', '%'.$search.'%');
                            })
                            ->paginate(10)->withQueryString();
        }else{
            $produkUmkm = ProdukUmkm::paginate(10);
        }
        return view('admin.produkUmkm.index', compact('produkUmkm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $daftarUmkm = DaftarUmkm::all();
        return view('admin.produkUmkm.create', compact('daftarUmkm'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'bpom' => ['required', 'string', 'max:255', 'unique:produk_umkm'],
            'nib' => ['required', 'string', 'max:255'],
            'nama_produk' => ['required', 'string', 'max:255'],
            'gambar_produk' => ['required'],
            'kategori' => ['required'],
            'sertifikasi_halal' => ['required'],
        ]);

        $daftarUmkm = DaftarUmkm::where('nib', $request->nib)->first();

        $request->request->add([
            'nama_usaha' => $daftarUmkm->nama_usaha,
            'alamat_usaha' => $daftarUmkm->alamat_usaha,
            'telp_usaha' => $daftarUmkm->telp_usaha,
        ]);
        
        $path = $request->file('gambar_produk')->storeAs(
            'produk' , $request->nama_produk.'_'.$request->bpom, 'public'
        );
        
        $store = $request->except(['gambar_produk']);
        
        $store['gambar_produk'] = $path;
        
        $produkUmkm = ProdukUmkm::create($store);
        
        return redirect()->route('admin.produkUmkm.show', $produkUmkm)->with('success', 'Berhasil menambah Produk UMKM <strong>'.$produkUmkm->nama_produk.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models ProdukUmkm  $produkUmkm
     * @return \Illuminate\Http\Response
     */
    public function show(ProdukUmkm $produkUmkm)
    {
        return view('admin.produkUmkm.show', compact('produkUmkm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models ProdukUmkm  $produkUmkm
     * @return \Illuminate\Http\Response
     */
    public function edit(ProdukUmkm $produkUmkm)
    {
        $daftarUmkm = DaftarUmkm::all();
        return view('admin.produkUmkm.edit', compact('produkUmkm', 'daftarUmkm'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models ProdukUmkm  $produkUmkm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProdukUmkm $produkUmkm)
    {
        $request->validate([
            'bpom' => ['required', 'string', 'max:255', 'unique:produk_umkm,bpom,'.$produkUmkm->id],
            'nib' => ['required', 'string', 'max:255'],
            'nama_produk' => ['required', 'string', 'max:255'],
            'kategori' => ['required'],
            'sertifikasi_halal' => ['required'],
        ]);

        if($request->nib != $produkUmkm->nib)
        {
            $daftarUmkm = DaftarUmkm::where('nib', $request->nib)->first();
    
            $request->request->add([
                'nama_usaha' => $daftarUmkm->nama_usaha,
                'alamat_usaha' => $daftarUmkm->alamat_usaha,
                'telp_usaha' => $daftarUmkm->telp_pemilik,
            ]);
        }
        
        if($request->has('gambar_produk')){
            Storage::disk('public')->delete($produkUmkm->gambar_produk);
            $path = $request->file('gambar_produk')->storeAs(
                'produk' , $request->nama_produk.'_'.$request->bpom, 'public'
            );
            $update = $request->except(['gambar_produk']);
            $update['gambar_produk'] = $path;
        }else{
            $update = $request->all();
        }

        $produkUnggulan = ProdukUnggulan::where('bpom', '=', $produkUmkm->bpom)->get();
        $bpom = $produkUmkm->bpom;
        $produkUmkm->update($request->all());
        
        if(count($produkUnggulan) > 0)
        {
            $produkUnggulanTable = (new ProdukUnggulan())->getTable();
            DB::table($produkUnggulanTable)->where('bpom', '=', $bpom)->update([
                'bpom' => $produkUmkm->bpom
            ]);
        }

        $produkUmkm->update($update);

        return redirect()->route('admin.produkUmkm.show', $produkUmkm)->with('success', 'Berhasil mengubah Produk UMKM <strong>'.$produkUmkm->nama_produk.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models ProdukUmkm  $produkUmkm
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProdukUmkm $produkUmkm)
    {
        $name = $produkUmkm->nama_produk;
        Storage::disk('public')->delete($produkUmkm->gambar_produk);
        ProdukUnggulan::where('bpom', '=', $produkUmkm->bpom)->delete();
        $produkUmkm->delete();

        return redirect()->route('admin.produkUmkm.index')->with('success', 'Berhasil menghapus Produk UMKM <strong>'.$name.'</strong>.');
    }
}
