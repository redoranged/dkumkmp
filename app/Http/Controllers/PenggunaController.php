<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('search'))
        {
            $pengguna = User::where('nik_pengguna', 'like', '%'.$request->search.'%')
                            ->orWhere('nama_pengguna', 'like', '%'.$request->search.'%')
                            ->paginate(10)->withQueryString();
        }else{
            $pengguna = User::paginate(10);
        }
        return view('admin.pengguna.index', compact('pengguna'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hak_akses == 'admin'){
            return view('admin.pengguna.create');
        }else{
            return redirect()->route('admin.pengguna.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nik_pengguna' => ['required', 'string', 'min:16', 'max:255', 'unique:pengguna'],
            'nama_pengguna' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'instansi' => ['required', 'string'],
            'hak_akses' => ['required', 'string'],
        ]);
        $store = $request->except(['password_confirmation']);
        $store['password'] = Hash::make($request->password);

        $pengguna = User::create($store);
        
        return redirect()->route('admin.pengguna.show', $pengguna)->with('success', 'Berhasil menambah pengguna <strong>'.$pengguna->nik_pengguna.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function show(User $pengguna)
    {
        return view('admin.pengguna.show', compact('pengguna'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function edit(User $pengguna)
    {
        return view('admin.pengguna.edit', compact('pengguna'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $pengguna)
    {
        $validator = [
            'nik_pengguna' => ['required', 'string', 'min:16', 'max:255', 'unique:pengguna,nik_pengguna,'.$pengguna->id],
            'nama_pengguna' => ['required', 'string', 'max:255'],
            'instansi' => ['required', 'string'],
            'hak_akses' => ['required', 'string'],
        ];
        
        if($request->has('password') && $request->password != null){
            $validator['password'] = ['required', 'string', 'min:8', 'confirmed'];
            $update = $request->except(['password_confirmation']);
            $update['password'] = Hash::make($request->password);
        }else{
            $update = $request->except(['password', 'password_confirmation']);
        }
        $request->validate($validator);

        $pengguna->update($update);
        
        return redirect()->route('admin.pengguna.show', $pengguna)->with('success', 'Berhasil mengubah pengguna <strong>'.$pengguna->nik_pengguna.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $pengguna
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $pengguna)
    {
        if(Auth::user()->hak_akses == 'admin'){
            $name = $pengguna->nik_pengguna;
            $pengguna->delete();
    
            return redirect()->route('admin.pengguna.index')->with('success', 'Berhasil menghapus pengguna <strong>'.$name.'</strong>.');
        }else{
            return redirect()->route('admin.pengguna.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }
}
