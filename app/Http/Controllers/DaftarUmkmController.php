<?php

namespace App\Http\Controllers;

use App\Models\DaftarUmkm;
use App\Models\ProdukUmkm;
use App\Models\ProdukUnggulan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DaftarUmkmController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if(Auth::user()->hak_akses == 'staf_dkumkmp')
        {
            if($request->has('search'))
            {
                $daftarUmkm = DaftarUmkm::where('nib', 'like', '%'.$request->search.'%')
                                ->whereNotNull('iumk')
                                ->orWhere('nama_pemilik', 'like', '%'.$request->search.'%')
                                ->whereNotNull('iumk')
                                ->orWhere('nama_usaha', 'like', '%'.$request->search.'%')
                                ->whereNotNull('iumk')
                                ->paginate(10)->withQueryString();
            }else{
                $daftarUmkm = DaftarUmkm::whereNotNull('iumk')->paginate(10);
            }
        }else{
            if($request->has('search'))
            {
                $daftarUmkm = DaftarUmkm::where('nib', 'like', '%'.$request->search.'%')
                                ->orWhere('nama_pemilik', 'like', '%'.$request->search.'%')
                                ->orWhere('nama_usaha', 'like', '%'.$request->search.'%')
                                ->paginate(10)->withQueryString();
            }else{
                $daftarUmkm = DaftarUmkm::paginate(10);
            }
        }
        return view('admin.daftarUmkm.index', compact('daftarUmkm'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(Auth::user()->hak_akses != 'staf_dkumkmp')
        {
            return view('admin.daftarUmkm.create');
        }else{
            return redirect()->route('admin.daftarUmkm.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nib' => ['required', 'string', 'max:255', 'unique:daftar_umkm'],
            'nama_pemilik' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'string', 'min:16', 'max:255'],
            'tgl_terbit' => ['required', 'date'],
            'kekayaan' => ['required', 'string', 'max:255'],
            'nama_usaha' => ['required', 'string', 'max:255'],
            'sektor' => ['required', 'string', 'max:255'],
            'kegiatan_usaha' => ['required', 'string', 'max:255'],
            'modal' => ['required', 'numeric'],
            'hasil' => ['required', 'numeric'],
            'jml_tenaga_kerja' => ['required', 'numeric'],
        ]);

        $daftarUmkm = DaftarUmkm::create($request->all());
        
        return redirect()->route('admin.daftarUmkm.show', $daftarUmkm)->with('success', 'Berhasil menambah Daftar UMKM <strong>'.$daftarUmkm->nib.'</strong>.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DaftarUmkm  $daftarUmkm
     * @return \Illuminate\Http\Response
     */
    public function show(DaftarUmkm $daftarUmkm)
    {
        return view('admin.daftarUmkm.show', compact('daftarUmkm'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DaftarUmkm  $daftarUmkm
     * @return \Illuminate\Http\Response
     */
    public function edit(DaftarUmkm $daftarUmkm)
    {
        if(Auth::user()->hak_akses != 'staf_dkumkmp')
        {
            return view('admin.daftarUmkm.edit', compact('daftarUmkm'));
        }else{
            return redirect()->route('admin.daftarUmkm.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DaftarUmkm  $daftarUmkm
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DaftarUmkm $daftarUmkm)
    {
        $request->validate([
            'nib' => ['required', 'string', 'max:255', 'unique:daftar_umkm,nib,'.$daftarUmkm->id],
            'nama_pemilik' => ['required', 'string', 'max:255'],
            'nik' => ['required', 'string', 'min:16', 'max:255'],
            'tgl_terbit' => ['required', 'date'],
            'kekayaan' => ['required'],
            'nama_usaha' => ['required', 'string', 'max:255'],
            'sektor' => ['required', 'string', 'max:255'],
            'kegiatan_usaha' => ['required', 'string', 'max:255'],
            'modal' => ['required', 'numeric'],
            'hasil' => ['required', 'numeric'],
            'jml_tenaga_kerja' => ['required', 'numeric'],
        ]);

        $produkUmkm = ProdukUmkm::where('nib', '=', $daftarUmkm->nib)->get();
        $nib = $daftarUmkm->nib;
        $daftarUmkm->update($request->all());
        
        if(count($produkUmkm) > 0)
        {
            $produkUmkmTable = (new ProdukUmkm())->getTable();
            DB::table($produkUmkmTable)->where('nib', '=', $nib)->update([
                'nib' => $daftarUmkm->nib
            ]);
        }

        return redirect()->route('admin.daftarUmkm.show', $daftarUmkm)->with('success', 'Berhasil mengubah Daftar UMKM <strong>'.$daftarUmkm->nib.'</strong>.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DaftarUmkm  $daftarUmkm
     * @return \Illuminate\Http\Response
     */
    public function destroy(DaftarUmkm $daftarUmkm)
    {
        if(Auth::user()->hak_akses == 'admin'){
            $name = $daftarUmkm->nib;
            $produkUmkm = ProdukUmkm::where('nib', '=', $daftarUmkm->nib)->get();
            if(count($produkUmkm) > 0)
            {
                foreach ($produkUmkm as $row) {
                    ProdukUnggulan::where('bpom', '=', $row->bpom)->delete();
                }
            }
            ProdukUmkm::where('nib', '=', $daftarUmkm->nib)->delete();
            $daftarUmkm->delete();
    
            return redirect()->route('admin.daftarUmkm.index')->with('success', 'Berhasil menghapus Daftar UMKM <strong>'.$name.'</strong>.');
        }else{
            return redirect()->route('admin.daftarUmkm.index')->with('warning', 'Anda tidak memiliki hak akses');
        }
    }
}
