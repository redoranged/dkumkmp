<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PengumumanBup extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pengumuman_bup';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_pengumuman_bup';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
}
