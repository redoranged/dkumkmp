<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProdukUnggulan extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'produk_unggulan';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id_unggulan';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * Get the produkUmkm for the produk.
     */
    public function produkUmkm()
    {
        return $this->belongsTo(ProdukUmkm::class, 'bpom', 'bpom');
    }
}
