<form action="{{ $action }}" method="{{ $method }}">
    @csrf
    @foreach ($inputItems as $input)
        @if ($input['type'] == 'text')
            <div class="form-group row">
                <label for="{{ $input['name'] }}" class="col-md-4 col-form-label text-md-right">{{ __($input['label']) }}{!! ($input['required']) ? '&nbsp;<span class="text-danger">*</span>' : '' !!}</label>

                <div class="col-md-6">
                    <input id="{{ $input['name'] }}" type="text" class="form-control @error($input['name']) is-invalid @enderror" name="{{ $input['name'] }}" value="{{ old($input['name']) }}" autocomplete="{{ $input['name'] }}" {{ ($input['required']) ? 'required' : '' }} {{ ($input['autofocus']) ? 'autofocus' : '' }}>

                    @error($input['name'])
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endif
        @if ($input['type'] == 'password')
            <div class="form-group row">
                <label for="{{ $input['name'] }}" class="col-md-4 col-form-label text-md-right">{{ __($input['label']) }}{!! ($input['required']) ? '&nbsp;<span class="text-danger">*</span>' : '' !!}</label>

                <div class="col-md-6">
                    <input id="{{ $input['name'] }}" type="password" class="form-control @error($input['name']) is-invalid @enderror" name="{{ $input['name'] }}" value="{{ old($input['name']) }}" {{ ($input['required']) ? 'required' : '' }} {{ ($input['autofocus']) ? 'autofocus' : '' }}>

                    @error($input['name'])
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endif
        @if ($input['type'] == 'textarea')
            <div class="form-group row">
                <label for="{{ $input['name'] }}" class="col-md-4 col-form-label text-md-right">{{ __($input['label']) }}{!! ($input['required']) ? '&nbsp;<span class="text-danger">*</span>' : '' !!}</label>

                <div class="col-md-6">
                    <textarea name="{{ $input['name'] }}" id="{{ $input['name'] }}" class="form-control @error($input['name']) is-invalid @enderror" {{ ($input['required']) ? 'required' : '' }} {{ ($input['autofocus']) ? 'autofocus' : '' }}>{{ old($input['name']) }}</textarea>

                    @error($input['name'])
                        <span class="invalid-feedback" role="alert">
                            <strong>{!! $message !!}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endif
        @if ($input['type'] == 'select')
            <div class="form-group row">
                <label for="{{ $input['name'] }}" class="col-md-4 col-form-label text-md-right">{{ __($input['label']) }}{!! ($input['required']) ? '&nbsp;<span class="text-danger">*</span>' : '' !!}</label>

                <div class="col-md-6">
                    <select name="{{ $input['name'] }}" id="{{ $input['name'] }}" class="form-control @error($input['name']) is-invalid @enderror" {{ ($input['required']) ? 'required' : '' }}>
                        <option selected disabled>-- {{ __($input['label']) }}--</option>
                    @foreach ($input['options'] as $option)
                        <option value="{{ $option['value'] }}" {{ ($option['selected']) ? 'selected' : '' }}>{{ $option['label'] }}</option>
                    @endforeach
                    </select>

                    @error($input['name'])
                        <span class="invalid-feedback" role="alert">
                            <strong>{!! $message !!}</strong>
                        </span>
                    @enderror
                </div>
            </div>
        @endif
    @endforeach

    @yield('form-additional')
    @stack('form-additional')

    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {!! __($submitButton ?? 'Submit') !!}
            </button>
        </div>
    </div>
</form>