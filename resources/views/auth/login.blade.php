@extends('layouts.app', [
    'page' => 'Login'
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    @include('components.form', [
                        'method' => 'POST',
                        'action' => route('login'),
                        'submitButton' => 'Login',
                        'inputItems' => [
                            [
                                'name' => 'nik_pengguna',
                                'type' => 'text',
                                'label' => 'NIK Pengguna',
                                'required' => true,
                                'autofocus' => true,
                            ],[
                                'name' => 'password',
                                'type' => 'password',
                                'label' => 'Password',
                                'required' => true,
                                'autofocus' => false,
                            ]
                        ]
                    ])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
