@extends('layouts.app', [
    'page' => 'Register'
])

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Register') }}</div>

                <div class="card-body">
                    @include('components.form', [
                        'method' => 'POST',
                        'action' => route('register'),
                        'submitButton' => 'Register',
                        'inputItems' => [
                            [
                                'name' => 'nik_pengguna',
                                'type' => 'text',
                                'label' => 'NIK Pengguna',
                                'required' => true,
                                'autofocus' => true,
                            ],[
                                'name' => 'nama_pengguna',
                                'type' => 'text',
                                'label' => 'Nama Pengguna',
                                'required' => true,
                                'autofocus' => false,
                            ],[
                                'name' => 'password',
                                'type' => 'password',
                                'label' => 'Password',
                                'required' => true,
                                'autofocus' => false,
                            ],[
                                'name' => 'password_confirmation',
                                'type' => 'password',
                                'label' => 'Confirm Password',
                                'required' => true,
                                'autofocus' => false,
                            ],[
                                'name' => 'instansi',
                                'type' => 'select',
                                'label' => 'Instansi',
                                'required' => true,
                                'autofocus' => false,
                                'options' => [
                                    [
                                        'value' => 'DKUMKMP',
                                        'label' => 'DKUMKMP',
                                        'selected' => false,
                                    ],
                                    [
                                        'value' => 'DPMPT',
                                        'label' => 'DPMPT',
                                        'selected' => false,
                                    ]
                                ]
                            ],[
                                'name' => 'no_telp',
                                'type' => 'text',
                                'label' => 'No Telepon',
                                'required' => false,
                                'autofocus' => false,
                            ],[
                                'name' => 'alamat',
                                'type' => 'textarea',
                                'label' => 'Alamat',
                                'required' => false,
                                'autofocus' => false,
                            ],[
                                'name' => 'hak_akses',
                                'type' => 'select',
                                'label' => 'Hak Akses',
                                'required' => true,
                                'autofocus' => false,
                                'options' => [
                                    [
                                        'value' => 'staf_dpmpt',
                                        'label' => 'Staf DPMPT',
                                        'selected' => false
                                    ],
                                    [
                                        'value' => 'staf_dkumkmp',
                                        'label' => 'Staf DKUMKMP',
                                        'selected' => false
                                    ],
                                    [
                                        'value' => 'admin',
                                        'label' => 'Admin',
                                        'selected' => false
                                    ],
                                ]
                            ]
                        ]
                    ])
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
