@extends('layouts.core', ['title' => $title ?? ''])

@push('css')

@endpush

@section('app')
    @include('layouts.components.header', ['public' => true])
    <div class="container-fluid">
        <div class="row pt-4">
            @yield('content')
        </div>
    </div>
@endsection