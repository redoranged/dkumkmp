<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? '' }}</title>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}">

    <!-- FontAwesome -->
    <script src="{{ asset('js/fontawesome.js') }}"></script>
    
    @yield('css')
    @stack('css')
</head>
<body>
    <div id="app">
        @yield('app')
    
    </div>
    
    <script>
        $(document).ready(function(){
            bsCustomFileInput.init();
            $('[data-toggle="tooltip"]').tooltip();
            $('.select2').select2();
        });
    </script>

    @if (session('success'))
        @include('components.toast', [
            'type' => 'success',
            'title' => 'Success',
            'message' => session('success')
        ])

        <script>
        $(document).ready(function(){
            $('#componentToast').toast('show');
        });</script>
    @endif

    @if (session('warning'))
        @include('components.toast', [
            'type' => 'warning',
            'title' => 'Warning',
            'message' => session('warning')
        ])

        <script>
        $(document).ready(function(){
            $('#componentToast').toast('show');
        });</script>
    @endif

    @if (session('error'))
        @include('components.toast', [
            'type' => 'danger',
            'title' => 'Error',
            'message' => session('error')
        ])

        <script>
        $(document).ready(function(){
            $('#componentToast').toast('show');
        });</script>
    @endif
    @yield('js')
    @stack('js')
</body>
</html>