@extends('layouts.core', ['title' => $title])

@push('css')
    <link rel="stylesheet" href="{{ asset('css/dashboard.css') }}"/>
@endpush

@section('app')
    @include('layouts.components.header', ['public' => false])
    <div class="container-fluid">
        <div class="row">
            @include('layouts.components.sidebar')

            <main class="col-md-9 ml-sm-auto col-lg-10 px-md-4 pt-2 mb-2">
                <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                    @if (isset($breadcrumbItems))
                        @include('components.breadcrumb', ['breadcrumbItems' => $breadcrumbItems])
                    @endif
                    
                    <div class="btn-toolbar mb-2 mb-md-0">
                        @yield('action-components')
                        @stack('action-components')
                    </div>
                </div>
                @yield('content')
            </main>
        </div>
    </div>
@endsection