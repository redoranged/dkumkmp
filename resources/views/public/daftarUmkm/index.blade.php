@php
    $title = 'Daftar UMKM';
    $page = 'Daftar UMKM';
@endphp

@extends('layouts.public', [
    'title' => $title,
    'page' => $page,
])

@section('content')
    <main class="container">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h2>{{ $page }}</h2>
            <div class="btn-toolbar mb-2 mb-md-0">
                <form action="{{ route('public.daftarUmkm') }}" method="get" class="input-group mr-3">
                    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
                    </div>
                </form>
            </div>
        </div>
        @include('public.daftarUmkm.view.table', [
            'data' => $daftarUmkm
        ])
    </main>
@endsection