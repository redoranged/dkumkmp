<div class="row mt-2 mb-2">
    @if (count($data) <= 0)
        <h5 class="col-md-12 text-center text-muted">Belum ada Pengumuman BUP!</h5>
    @endif
    @foreach ($data as $row)
            <div class="offset-md-2 col-md-8 mb-3">
                <div class="card">
                    <div class="card-body row">
                        <div class="offset-md-1 col-md-10 col-sm-12">
                            <h3 class="card-title">{{ $row->judul_pengumuman }}</h3>
                            <span class="card-subtitle">{!! $row->deskripsi !!}</span>
                            <p class="text-muted">
                                <a href="{{ route('public.pengumumanBup.download', $row) }}"><i class="fas fa-download"></i> &nbsp;Pengumuman_{{$row->judul_pengumuman}}.pdf</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
    @endforeach
    </div>
    
    @include('components.pagination', ['data' => $data])