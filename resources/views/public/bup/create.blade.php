@php
    $title = 'BUP';
    $page = 'BUP';
@endphp

@extends('layouts.public', [
    'title' => $title,
    'page' => $page,
])

@section('content')
    <main class="container">
        <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
            <h2>Pengajuan {{ $page }}</h2>
            <div class="btn-toolbar mb-2 mb-md-0">

            </div>
            <div class="btn-group me-2">
                <a href="{{ route('public.bup') }}" class="btn btn-secondary mr-1">Discard</a>
                <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Save</button>
            </div>
        </div>
        @include('public.bup.view.form', ['bup' => false, 'form_open' => true])
    </main>
@endsection
@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-bup').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush