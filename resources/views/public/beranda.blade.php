@extends('layouts.public', [
    'title' => 'Beranda',
    'page' => 'Beranda',
])

@section('content')
    <main class="container">
        <div class="card">
            <div class="card-header">
                Selamat Datang!
            </div>
            <div class="card-body">
                Profil DKUMKMP
            </div>
        </div>
    </main>
@endsection