<div class="row mt-2 mb-2">
@if (count($data) <= 0)
    <h5 class="col-md-12 text-center text-muted">Belum ada Jadwal Pameran!</h5>
@endif
@foreach ($data as $row)
        <div class="offset-md-2 col-md-8 mb-3">
            <div class="card">
                <div class="card-body row">
                    <div class="col-md-5 col-sm-12 p-0 mt-1">
                        <img class="col-12 m-0" src="{{ asset('storage/'.$row->poster_pameran) }}"/>
                    </div>
                    <div class="col-md-7 col-sm-12 ml-0 pl-0 pt-1">
                        <h3 class="card-title">{{ $row->nama_pameran }}</h3>
                        <span class="card-subtitle">{!! $row->lokasi_pameran !!}</span> | 
                        <span class="badge badge-primary text-wrap">{{ $row->tgl_pameran }}</span>
                        <p class="text-muted">{!! $row->deskripsi_pameran !!}</p>
                    </div>
                </div>
            </div>
        </div>
@endforeach
</div>

@include('components.pagination', ['data' => $data])