@php
    $hak_akses = [
        'admin' => 'Admin',
        'staf_dpmpt' => 'Staf DPMPT',
        'staf_dkumkmp' => 'Staf DKUMKMP'
    ];
@endphp 
@extends('layouts.dashboard',[
    'title' => 'Beranda '.$hak_akses[Auth::user()->hak_akses],
    'page' => 'Beranda',
    'pageTitle' => 'Beranda',
])

@push('action-components')
    <h2>Beranda {{ $hak_akses[Auth::user()->hak_akses] }}</h2>
@endpush

@section('content')
    <div class="row">
        <div class="offset-md-1 col-md-10 offset-sm-1 col-sm-10 border border-secondary rounded text-center p-3">
            <h2>Selamat Datang di Halaman {{ $hak_akses[Auth::user()->hak_akses] }}!</h2>
            <a href="{{ route('admin.pengguna.edit', Auth::user()->id) }}" class="btn btn-primary text-white mt-2">Edit Profile</a>
            <div class="row mt-3">
                <div class="offset-sm-1 col-sm-10 offset-md-3 col-md-6 text-left">
                    <div class="row mt-1">
                        <div class="col-5">
                            NIK
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ Auth::user()->nik_pengguna }}
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-5">
                            Nama
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ Auth::user()->nama_pengguna }}
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-5">
                            Instansi
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ Auth::user()->instansi }}
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-5">
                            Nomor Telepon
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ Auth::user()->no_telp }}
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-5">
                            Alamat
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ Auth::user()->alamat }}
                        </div>
                    </div>
                    <div class="row mt-1">
                        <div class="col-5">
                            Hak Akses
                        </div>
                        <div class="col-1 text-center">
                            :
                        </div>
                        <div class="col-6">
                            {{ $hak_akses[Auth::user()->hak_akses] }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
