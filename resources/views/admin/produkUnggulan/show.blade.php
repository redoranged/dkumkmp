@php
    $title = 'Produk Unggulan';
    $page = 'Produk Unggulan';
    $pageTitle = 'Produk Unggulan';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Produk Unggulan',
            'link' => route('admin.produkUnggulan.index'),
        ],
        [
            'name' => $produkUnggulan->produkUmkm->nama_produk,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.produkUnggulan.index') }}" class="btn btn-secondary mr-1">Back</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <a href="{{ route('admin.produkUnggulan.edit', $produkUnggulan) }}" class="btn btn-primary">Edit</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.produkUnggulan.view.form', ['produkUnggulan' => $produkUnggulan, 'form_open' => false])
@endsection