@php
    $title = 'Pengajuan BUP';
    $page = 'Pengajuan BUP';
    $pageTitle = 'Pengajuan BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pengajuan BUP',
            'link' => route('admin.bup.index'),
        ],
        [
            'name' => $bup->nib.' ('.$bup->nama_pemilik.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.bup.index') }}" class="btn btn-secondary mr-1">Back</a>
    <a href="{{ route('admin.bup.edit', $bup) }}" class="btn btn-primary">Edit</a>
</div>
@endpush

@section('content')
    @include('admin.bup.view.form', ['bup' => $bup, 'form_open' => false])
@endsection