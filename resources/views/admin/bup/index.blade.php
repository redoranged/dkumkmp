@php
    $page = 'Pengajuan BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Pengajuan BUP',
    'page' => $page,
    'pageTitle' => 'Pengajuan BUP',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.bup.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    <a href="{{ route('admin.bup.create') }}" class="btn btn-outline-success">Create</a>
</div>
@endpush

@section('content')
    @include('admin.bup.view.table', [
        'data' => $bup
    ])
@endsection