<form id="form-daftarUmkm" action="{{ ($daftarUmkm) ? route('admin.daftarUmkm.update', $daftarUmkm) : route('admin.daftarUmkm.store') }}"id="form-daftarUmkm" method="POST">
    @csrf
    <input type="hidden" name="_method" value="{{ ($daftarUmkm) ? 'PUT' : 'POST' }}" />

    <div class="form-group row">
        <label for="nib" class="col-md-4 col-form-label text-md-right">{{ __('NIB') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nib" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nib') is-invalid @enderror" name="nib" value="{{ (old('nib')) ? old('nib') : $daftarUmkm->nib ?? '' }}" autocomplete="nib" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nib')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_pemilik" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pemilik') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_pemilik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_pemilik') is-invalid @enderror" name="nama_pemilik" value="{{ (old('nama_pemilik')) ? old('nama_pemilik') : $daftarUmkm->nama_pemilik ?? '' }}" autocomplete="nama_pemilik" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('nama_pemilik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nik" class="col-md-4 col-form-label text-md-right">{{ __('NIK') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nik') is-invalid @enderror" name="nik" value="{{ (old('nik')) ? old('nik') : $daftarUmkm->nik ?? '' }}" autocomplete="nik" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('nik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="iumk" class="col-md-4 col-form-label text-md-right">{{ __('IUMK') }}</label>

        <div class="col-md-6">
            <input id="iumk" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('iumk') is-invalid @enderror" name="iumk" value="{{ (old('iumk')) ? old('iumk') : $daftarUmkm->iumk ?? '' }}" autocomplete="iumk" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('iumk')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="tgl_terbit" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Terbit') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="tgl_terbit" type="date" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('tgl_terbit') is-invalid @enderror" name="tgl_terbit" value="{{ (old('tgl_terbit')) ? old('tgl_terbit') : $daftarUmkm->tgl_terbit ?? '' }}" autocomplete="tgl_terbit" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('tgl_terbit')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kekayaan" class="col-md-4 col-form-label text-md-right">{{ __('Kekayaan') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="kekayaan" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('kekayaan') is-invalid @enderror" name="kekayaan" value="{{ (old('kekayaan')) ? old('kekayaan') : $daftarUmkm->kekayaan ?? '' }}" autocomplete="kekayaan" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('kekayaan')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_usaha" class="col-md-4 col-form-label text-md-right">{{ __('Nama Usaha') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_usaha" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_usaha') is-invalid @enderror" name="nama_usaha" value="{{ (old('nama_usaha')) ? old('nama_usaha') : $daftarUmkm->nama_usaha ?? '' }}" autocomplete="nama_usaha" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nama_usaha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="sektor" class="col-md-4 col-form-label text-md-right">{{ __('Sektor') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="sektor" class="form-control @error('sektor') is-invalid @enderror" name="sektor" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Sektor --</option>
                <option value="Fashion" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Fashion") ? 'selected' : '' }}>Fashion</option>
                <option value="Teknologi" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Teknologi") ? 'selected' : '' }}>Teknologi</option>
                <option value="Kuliner" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Kuliner") ? 'selected' : '' }}>Kuliner</option>
                <option value="Cinderamata" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Cinderamata") ? 'selected' : '' }}>Cinderamata</option>
                <option value="Kosmetik" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Kosmetik") ? 'selected' : '' }}>Kosmetik</option>
                <option value="Agro Bisnis" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Agro Bisnis") ? 'selected' : '' }}>Agro Bisnis</option>
                <option value="Otomotif" {{ (((old('sektor')) ? old('sektor') : $daftarUmkm->sektor ?? '') == "Otomotif") ? 'selected' : '' }}>Otomotif</option>
            </select>

            @error('sektor')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kbli" class="col-md-4 col-form-label text-md-right">{{ __('KBLI') }}</label>

        <div class="col-md-6">
            <input id="kbli" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('kbli') is-invalid @enderror" name="kbli" value="{{ (old('kbli')) ? old('kbli') : $daftarUmkm->kbli ?? '' }}" autocomplete="kbli" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('kbli')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="kegiatan_usaha" class="col-md-4 col-form-label text-md-right">{{ __('Kegiatan Usaha') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="kegiatan_usaha" class="form-control @error('kegiatan_usaha') is-invalid @enderror" name="kegiatan_usaha" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Kegiatan Usaha --</option>
                <option value="Fashion" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Fashion") ? 'selected' : '' }}>Fashion</option>
                <option value="Teknologi" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Teknologi") ? 'selected' : '' }}>Teknologi</option>
                <option value="Kuliner" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Kuliner") ? 'selected' : '' }}>Kuliner</option>
                <option value="Cinderamata" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Cinderamata") ? 'selected' : '' }}>Cinderamata</option>
                <option value="Kosmetik" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Kosmetik") ? 'selected' : '' }}>Kosmetik</option>
                <option value="Agro Bisnis" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Agro Bisnis") ? 'selected' : '' }}>Agro Bisnis</option>
                <option value="Otomotif" {{ (((old('kegiatan_usaha')) ? old('kegiatan_usaha') : $daftarUmkm->kegiatan_usaha ?? '') == "Otomotif") ? 'selected' : '' }}>Otomotif</option>
            </select>

            @error('kegiatan_usaha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="alamat_usaha" class="col-md-4 col-form-label text-md-right">{{ __('Alamat Usaha') }}</label>

        <div class="col-md-6">
            <textarea id="alamat_usaha" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('alamat_usaha') is-invalid @enderror" name="alamat_usaha" autocomplete="alamat_usaha" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('alamat_usaha')) ? old('alamat_usaha') : $daftarUmkm->alamat_usaha ?? '' }}</textarea>

            @error('alamat_usaha')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="telp_pemilik" class="col-md-4 col-form-label text-md-right">{{ __('Telepon Pemilik') }}</label>

        <div class="col-md-6">
            <input id="telp_pemilik" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('telp_pemilik') is-invalid @enderror" name="telp_pemilik" value="{{ (old('telp_pemilik')) ? old('telp_pemilik') : $daftarUmkm->telp_pemilik ?? '' }}" autocomplete="telp_pemilik" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('telp_pemilik')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="modal" class="col-md-4 col-form-label text-md-right">{{ __('Modal') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="modal" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('modal') is-invalid @enderror" name="modal" value="{{ (old('modal')) ? old('modal') : $daftarUmkm->modal ?? '' }}" autocomplete="modal" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('modal')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="hasil" class="col-md-4 col-form-label text-md-right">{{ __('Hasil') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="hasil" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('hasil') is-invalid @enderror" name="hasil" value="{{ (old('hasil')) ? old('hasil') : $daftarUmkm->hasil ?? '' }}" autocomplete="hasil" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('hasil')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="jml_tenaga_kerja" class="col-md-4 col-form-label text-md-right">{{ __('Jumlah Tenaga Kerja') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="jml_tenaga_kerja" type="number" min="1" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('jml_tenaga_kerja') is-invalid @enderror" name="jml_tenaga_kerja" value="{{ (old('jml_tenaga_kerja')) ? old('jml_tenaga_kerja') : $daftarUmkm->jml_tenaga_kerja ?? '' }}" autocomplete="jml_tenaga_kerja" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('jml_tenaga_kerja')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="npwp" class="col-md-4 col-form-label text-md-right">{{ __('NPWP') }}</label>

        <div class="col-md-6">
            <input id="npwp" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('npwp') is-invalid @enderror" name="npwp" value="{{ (old('npwp')) ? old('npwp') : $daftarUmkm->npwp ?? '' }}" autocomplete="npwp" {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('npwp')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <button type="submit" style="display:none">SUBMIT</button>
</form>