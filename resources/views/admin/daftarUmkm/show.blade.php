@php
    $title = 'Daftar UMKM';
    $page = 'Daftar UMKM';
    $pageTitle = 'Daftar UMKM';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Daftar UMKM',
            'link' => route('admin.daftarUmkm.index'),
        ],
        [
            'name' => $daftarUmkm->nib.' ('.$daftarUmkm->nama_usaha.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.daftarUmkm.index') }}" class="btn btn-secondary mr-1">Back</a>
    @if (Auth::user()->hak_akses != 'staf_dkumkmp')
        <a href="{{ route('admin.daftarUmkm.edit', $daftarUmkm) }}" class="btn btn-primary">Edit</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.daftarUmkm.view.form', ['daftarUmkm' => $daftarUmkm, 'form_open' => false])
@endsection