@php
    $page = 'Produk UMKM';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Produk UMKM',
    'page' => $page,
    'pageTitle' => 'Produk UMKM',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.produkUmkm.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <a href="{{ route('admin.produkUmkm.create') }}" class="btn btn-outline-success">Create</a>
    @endif
</div>
@endpush

@section('content')
    {{-- Kanban --}}
    @include('admin.produkUmkm.view.kanban', [
        'data' => $produkUmkm
    ])
    {{-- Table --}}
    {{-- @include('admin.produkUmkm.view.table', [
        'data' => $produkUmkm
    ]) --}}
@endsection