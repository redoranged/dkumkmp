
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>BPOM</th>
                <th>Nama Produk</th>
                <th>Gambar Produk</th>
                <th>Kategori</th>
                <th>Deskripsi</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="7" class="text-center text-muted">Belum ada Produk UMKM!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.produkUmkm.show', $row) }}">{{ $row->bpom }}</a></td>
                <td>{{ $row->nama_produk }}</td>
                <td>{{ $row->gambar_produk }}</td>
                <td>{{ $row->kategori }}</td>
                <td>{{ $row->deskripsi_produk }}</td>
                <td>
                @if (Auth::user()->hak_akses != 'staf_dpmpt')
                    <form action="{{ route('admin.produkUmkm.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nama_produk }}?');" method="POST">
                        @method('delete')
                        @csrf
                        <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                    </form>
                @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
@include('components.pagination', ['data' => $data])