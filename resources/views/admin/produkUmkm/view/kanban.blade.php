<div class="row mt-2 mb-2">
@if (count($data) <= 0)
    <h5 class="col-md-12 text-center text-muted">Belum ada Produk UMKM!</h5>
@endif
@foreach ($data as $row)
        <div class="col-md-3 col-sm-6 mb-3">
            <div class="card">
                <div class="card-body row">
                    <div class="col-12 m-0 mt-1 p-0">
                        <img class="col-12 m-0" style="height:125px" src="{{ asset('storage/'.$row->gambar_produk) }}"/>
                    </div>
                    <div class="col-12 pt-1 mt-1" style="height:200px">
                        <h5 class="card-title">{{ $row->nama_produk }} <span class="text-muted small">({{ $row->daftarUmkm->nama_usaha}})</span></h5>
                        <span class="card-subtitle">{!! $row->bpom !!}</span> | 
                        <span class="badge badge-primary text-wrap">{{ $row->kategori }}</span>
                        <p>{!! $row->daftarUmkm->telp_pemilik.' - '.$row->daftarUmkm->alamat_usaha !!}</p>
                        <p class="text-muted" data-toggle="tooltip" data-placement="bottom" title="{!! $row->deskripsi_produk !!}">{!! substr($row->deskripsi_produk, 0, 75) !!}...</p>
                        <div class="row mt-2">
                            <a href="{{ route('admin.produkUmkm.show', $row) }}" class="col-4 card-link">Detail</a>
                        @if (Auth::user()->hak_akses != 'staf_dpmpt')
                            <form action="{{ route('admin.produkUmkm.destroy', $row) }}" class="col-8 text-right" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nama_produk }}?');" method="POST">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="fas fa-trash"></i></button>
                            </form>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endforeach
</div>

@include('components.pagination', ['data' => $data])