@php
    $title = 'Produk UMKM';
    $page = 'Produk UMKM';
    $pageTitle = 'Produk UMKM';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Produk UMKM',
            'link' => route('admin.produkUmkm.index'),
        ],
        [
            'name' => $produkUmkm->nama_produk,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.produkUmkm.index') }}" class="btn btn-secondary mr-1">Back</a>
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <a href="{{ route('admin.produkUmkm.edit', $produkUmkm) }}" class="btn btn-primary">Edit</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.produkUmkm.view.form', ['produkUmkm' => $produkUmkm, 'form_open' => false])
@endsection