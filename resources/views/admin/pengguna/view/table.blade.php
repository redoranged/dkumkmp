
<div class="table-responsive">
    <table class="table table-bordered table-striped table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>NIK Pengguna</th>
                <th>Nama Pengguna</th>
                <th>Instansi</th>
                <th>No Telepon</th>
                <th>Alamat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
        @if (count($data) <= 0)
            <tr>
                <td colspan="7" class="text-center text-muted">Belum ada Pengguna!</td>
            </tr>
        @endif
        @foreach ($data as $row)
            <tr>
                <td>{{ $loop->iteration }}</td>
                <td><a href="{{ route('admin.pengguna.show', $row) }}">{{ $row->nik_pengguna }}</a></td>
                <td>{{ $row->nama_pengguna }}</td>
                <td>{{ $row->instansi }}</td>
                <td>{{ $row->no_telp }}</td>
                <td>{{ $row->alamat }}</td>
                <td>
                    @if ($row->id != Auth::user()->id && Auth::user()->hak_akses == 'admin')
                        <form action="{{ route('admin.pengguna.destroy', $row) }}" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nik_pengguna }}?');" method="POST">
                            @method('delete')
                            @csrf
                            <button type="submit" class="btn btn-sm btn-danger"><i class="fas fa-trash"></i></button>
                        </form>
                    @endif
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>    
@include('components.pagination', ['data' => $data])