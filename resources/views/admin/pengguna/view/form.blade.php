<form id="form-pengguna" action="{{ ($pengguna) ? route('admin.pengguna.update', $pengguna) : route('admin.pengguna.store') }}" method="POST">
    @csrf
    <input type="hidden" name="_method" value="{{ ($pengguna) ? 'PUT' : 'POST' }}" />

    <div class="form-group row">
        <label for="nik_pengguna" class="col-md-4 col-form-label text-md-right">{{ __('NIK Pengguna') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nik_pengguna" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nik_pengguna') is-invalid @enderror" name="nik_pengguna" value="{{ (old('nik_pengguna')) ? old('nik_pengguna') : $pengguna->nik_pengguna ?? '' }}" autocomplete="nik_pengguna" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nik_pengguna')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="nama_pengguna" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pengguna') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_pengguna" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_pengguna') is-invalid @enderror" name="nama_pengguna" value="{{ (old('nama_pengguna')) ? old('nama_pengguna') : $pengguna->nama_pengguna ?? '' }}" autocomplete="nama_pengguna" required {{ ($form_open) ? '' : 'readonly' }}>

            @error('nama_pengguna')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @if($form_open)
        <div class="form-group row">
            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }} {!! ($pengguna) ? '' : '&nbsp;<span class="text-danger">*</span>' !!}</label>

            <div class="col-md-6">
                <input id="password" type="password" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('password') is-invalid @enderror" name="password" value="" autocomplete="password" {{ ($pengguna) ? '' : 'required' }} {{ ($form_open) ? '' : 'readonly' }}>

                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
        <div class="form-group row">
            <label for="password_confirmation" id="label_password_confirmation" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }} {!! ($pengguna) ? '' : '&nbsp;<span class="text-danger">*</span>' !!}</label>

            <div class="col-md-6">
                <input id="password_confirmation" type="password" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('password_confirmation') is-invalid @enderror" name="password_confirmation" value="{{ (old('password_confirmation')) ? old('password_confirmation') : $pengguna->password_confirmation ?? '' }}" autocomplete="password_confirmation" {{ ($pengguna) ? '' : 'required' }} {{ ($form_open) ? '' : 'readonly' }}>

                @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endif
    <div class="form-group row">
        <label for="instansi" class="col-md-4 col-form-label text-md-right">{{ __('Instansi') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <select id="instansi" class="form-control @error('instansi') is-invalid @enderror" name="instansi" required {{ ($form_open) ? '' : 'disabled' }}>
                <option value="" disabled selected>-- Instansi --</option>
                <option value="DKUMKMP" {{ (((old('instansi')) ? old('instansi') : $pengguna->instansi ?? '') == "DKUMKMP") ? 'selected' : '' }}>DKUMKMP</option>
                <option value="DPMPT" {{ (((old('instansi')) ? old('instansi') : $pengguna->instansi ?? '') == "DPMPT") ? 'selected' : '' }}>DPMPT</option>
            </select>

            @error('instansi')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="no_telp" class="col-md-4 col-form-label text-md-right">{{ __('Nomor Telepon') }}</label>

        <div class="col-md-6">
            <input id="no_telp" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('no_telp') is-invalid @enderror" name="no_telp" value="{{ (old('no_telp')) ? old('no_telp') : $pengguna->no_telp ?? '' }}" autocomplete="no_telp" {{ ($form_open) ? '' : 'readonly' }}>

            @error('no_telp')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="alamat" class="col-md-4 col-form-label text-md-right">{{ __('Alamat') }}</label>

        <div class="col-md-6">
            <textarea id="alamat" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('alamat') is-invalid @enderror" name="alamat" autocomplete="alamat" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('alamat')) ? old('alamat') : $pengguna->alamat ?? '' }}</textarea>

            @error('alamat')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    @if (Auth::user()->hak_akses == 'admin')
        <div class="form-group row">
            <label for="hak_akses" class="col-md-4 col-form-label text-md-right">{{ __('Hak Akses') }} &nbsp;<span class="text-danger">*</span></label>

            <div class="col-md-6">
                <select id="hak_akses" class="form-control @error('hak_akses') is-invalid @enderror" name="hak_akses" required {{ ($form_open) ? '' : 'disabled' }}>
                    <option id="hak_akses_disabled" value="" disabled selected>-- Hak Akses --</option>
                    <option id="hak_akses_staf_dpmpt" value="staf_dpmpt" {{ (((old('hak_akses')) ? old('hak_akses') : $pengguna->hak_akses ?? '') == "staf_dpmpt") ? 'selected' : '' }}>Staf DPMPT</option>
                    <option id="hak_akses_staf_dkumkmp" value="staf_dkumkmp" {{ (((old('hak_akses')) ? old('hak_akses') : $pengguna->hak_akses ?? '') == "staf_dkumkmp") ? 'selected' : '' }}>Staf DKUMKMP</option>
                    <option id="hak_akses_admin" value="admin" {{ (((old('hak_akses')) ? old('hak_akses') : $pengguna->hak_akses ?? '') == "admin") ? 'selected' : '' }}>Admin</option>
                </select>

                @error('hak_akses')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
            </div>
        </div>
    @endif
    <button type="submit" style="display:none">SUBMIT</button>
</form>

<script>
    $(document).ready(function() {
        $('#instansi').on('change keyup focus', function() {
            var value = $(this).val();
            console.log(value)
            if(value == 'DKUMKMP'){
                $('#hak_akses_disabled').show().prop('selected', true)
                $('#hak_akses_staf_dpmpt').hide()
                $('#hak_akses_staf_dkumkmp').show()
                $('#hak_akses_admin').show()
            }else if(value == 'DPMPT'){
                $('#hak_akses_staf_dpmpt').show().prop('selected', true)
                $('#hak_akses_staf_dkumkmp').hide()
                $('#hak_akses_admin').hide()
            }else{
                $('#hak_akses_disabled').show().prop('selected', true)
                $('#hak_akses_staf_dpmpt').hide()
                $('#hak_akses_staf_dkumkmp').hide()
                $('#hak_akses_admin').hide()
            }
        });
    });
</script>
@if ($pengguna && $form_open)
    <script>
        $(document).ready(function(){
            $('#password').on('change keyup focus', function() {
                var password = $(this).val();
                if(password.length > 0){
                    $('#password_confirmation').prop('required', true);
                    $('#label_password_confirmation').html('Confirm Password &nbsp;<span class="text-danger">*</span>');
                }else{
                    $('#password_confirmation').prop('required', false);
                    $('#label_password_confirmation').html('Confirm Password');
                }
            });

            
        });
    </script>
@endif