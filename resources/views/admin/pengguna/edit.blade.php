@php
    $title = 'Daftar Pengguna';
    $page = 'Daftar Pengguna';
    $pageTitle = 'Daftar Pengguna';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Daftar Pengguna',
            'link' => route('admin.pengguna.index'),
        ],
        [
            'name' => $pengguna->nik_pengguna.' ('.$pengguna->nama_pengguna.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    @if (Auth::user()->hak_akses == 'admin')
        <a href="{{ route('admin.pengguna.show', $pengguna) }}" class="btn btn-secondary mr-1">Back</a>
    @endif
    <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Update</button>
</div>
@endpush

@section('content')
    @include('admin.pengguna.view.form', ['pengguna' => $pengguna, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-pengguna').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush