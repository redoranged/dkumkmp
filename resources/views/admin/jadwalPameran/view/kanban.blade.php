<div class="row mt-2 mb-2">
@if (count($data) <= 0)
    <h5 class="col-md-12 text-center text-muted">Belum ada Jadwal Pameran!</h5>
@endif
@foreach ($data as $row)
        <div class="offset-md-2 col-md-8 mb-3">
            <div class="card">
                <div class="card-body row">
                    <div class="col-md-5 col-sm-12 p-0 mt-1">
                        <img class="col-12 m-0" src="{{ asset('storage/'.$row->poster_pameran) }}"/>
                    </div>
                    <div class="col-md-7 col-sm-12 ml-0 pl-0 pt-1">
                        <h3 class="card-title">{{ $row->nama_pameran }}</h3>
                        <span class="card-subtitle">{!! $row->lokasi_pameran !!}</span> | 
                        <span class="badge badge-primary text-wrap">{{ $row->tgl_pameran }}</span>
                        <p class="text-muted">{!! $row->deskripsi_pameran !!}</p>
                        <div class="row mt-2">
                            <a href="{{ route('admin.jadwalPameran.show', $row) }}" class="col-4 card-link">Detail</a>
                        @if (Auth::user()->hak_akses != 'staf_dpmpt')
                            <form action="{{ route('admin.jadwalPameran.destroy', $row) }}" class="col-8 text-right" id="form-delete" onsubmit="return confirm('Apakah anda yakin untuk menghapus {{ $row->nama_pameran }}?');" method="POST">
                                @method('delete')
                                @csrf
                                <button type="submit" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="bottom" title="Hapus"><i class="fas fa-trash"></i></button>
                            </form>
                        @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endforeach
</div>

@include('components.pagination', ['data' => $data])