<form id="form-jadwalPameran" action="{{ ($jadwalPameran) ? route('admin.jadwalPameran.update', $jadwalPameran) : route('admin.jadwalPameran.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ ($jadwalPameran) ? 'PUT' : 'POST' }}" />
    <div class="form-group row">
        <label for="nama_pameran" class="col-md-4 col-form-label text-md-right">{{ __('Nama Pameran') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="nama_pameran" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('nama_pameran') is-invalid @enderror" name="nama_pameran" value="{{ (old('nama_pameran')) ? old('nama_pameran') : $jadwalPameran->nama_pameran ?? '' }}" autocomplete="nama_pameran" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('nama_pameran')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="tgl_pameran" class="col-md-4 col-form-label text-md-right">{{ __('Tanggal Pameran') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="tgl_pameran" type="date" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('tgl_pameran') is-invalid @enderror" name="tgl_pameran" value="{{ (old('tgl_pameran')) ? old('tgl_pameran') : $jadwalPameran->tgl_pameran ?? '' }}" autocomplete="tgl_pameran" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('tgl_pameran')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="lokasi_pameran" class="col-md-4 col-form-label text-md-right">{{ __('Lokasi Pameran') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <textarea id="lokasi_pameran" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('lokasi_pameran') is-invalid @enderror" name="lokasi_pameran" autocomplete="lokasi_pameran" {{ ($form_open) ? '' : 'readonly' }} required>{{ (old('lokasi_pameran')) ? old('lokasi_pameran') : $jadwalPameran->lokasi_pameran ?? '' }}</textarea>

            @error('lokasi_pameran')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="deskripsi_pameran" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi') }}</label>

        <div class="col-md-6">
            <textarea id="deskripsi_pameran" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('deskripsi_pameran') is-invalid @enderror" name="deskripsi_pameran" autocomplete="deskripsi_pameran" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('deskripsi_pameran')) ? old('deskripsi_pameran') : $jadwalPameran->deskripsi_pameran ?? '' }}</textarea>

            @error('deskripsi_pameran')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="poster_pameran" class="col-md-4 col-form-label text-md-right">{{ __('Poster Pameran') }}{!! ($jadwalPameran) ? '' : '&nbsp;<span class="text-danger">*</span>' !!}</label>

        <div class="col-md-6">
            @if ($form_open)
            <div class="custom-file">
                <input id="poster_pameran" type="file" accept="image/*" class="{{ ($form_open) ? 'custom-file-input' : 'form-control-plaintext' }} @error('poster_pameran') is-invalid @enderror" name="poster_pameran" value="{{ (old('poster_pameran')) ? old('poster_pameran') : $jadwalPameran->poster_pameran ?? '' }}" autocomplete="poster_pameran" {{ ($jadwalPameran) ? '' : 'required' }} {{ ($form_open) ? 'autofocus' : 'readonly' }}>
                <label class="custom-file-label" for="poster_pameran">Choose file</label>
            </div>
            @endif
            <img id="previewImg" src="{{ ($jadwalPameran) ? asset('storage/'.$jadwalPameran->poster_pameran) : '#' }}" class="mt-3 col-12"/>
            @error('poster_pameran')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    
    <button type="submit" style="display:none">SUBMIT</button>
</form>

@if ($form_open)
    <script>
        $(document).ready(function(){
            $('#poster_pameran').on('change keyup focus', function(e){
                if (this.files && this.files[0]) {
                    var reader = new FileReader();
                    reader.onload = function (e) {
                        $('#previewImg').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(this.files[0]);
                }
            });
        });
    </script>
@endif