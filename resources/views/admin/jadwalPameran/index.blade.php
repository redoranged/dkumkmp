@php
    $page = 'Pameran';
@endphp

@extends('layouts.dashboard',[
    'title' => 'Pameran',
    'page' => $page,
    'pageTitle' => 'Pameran',
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => $page,
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<form action="{{ route('admin.jadwalPameran.index') }}" method="get" class="input-group mr-3">
    <input type="text" name="search" id="search" value="{{ $_GET['search'] ?? '' }}" placeholder="Search..." class="form-control"/>
    <div class="input-group-append">
        <button type="submit" class="btn btn-info text-white"><i class="fas fa-search"></i></button>
    </div>
</form>
<div class="btn-group me-2">
    @if (Auth::user()->hak_akses != 'staf_dpmpt')
        <a href="{{ route('admin.jadwalPameran.create') }}" class="btn btn-outline-success">Create</a>
    @endif
</div>
@endpush

@section('content')
    @include('admin.jadwalPameran.view.kanban', [
        'data' => $jadwalPameran
    ])
@endsection