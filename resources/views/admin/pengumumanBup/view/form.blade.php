<form id="form-pengumumanBup" action="{{ ($pengumumanBup) ? route('admin.pengumumanBup.update', $pengumumanBup) : route('admin.pengumumanBup.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <input type="hidden" name="_method" value="{{ ($pengumumanBup) ? 'PUT' : 'POST' }}" />
    <div class="form-group row">
        <label for="judul_pengumuman" class="col-md-4 col-form-label text-md-right">{{ __('Judul Pengumuman') }} &nbsp;<span class="text-danger">*</span></label>

        <div class="col-md-6">
            <input id="judul_pengumuman" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('judul_pengumuman') is-invalid @enderror" name="judul_pengumuman" value="{{ (old('judul_pengumuman')) ? old('judul_pengumuman') : $pengumumanBup->judul_pengumuman ?? '' }}" autocomplete="judul_pengumuman" required {{ ($form_open) ? 'autofocus' : 'readonly' }}>

            @error('judul_pengumuman')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="deskripsi" class="col-md-4 col-form-label text-md-right">{{ __('Deskripsi') }}</label>

        <div class="col-md-6">
            <textarea id="deskripsi" type="text" class="{{ ($form_open) ? 'form-control' : 'form-control-plaintext' }} @error('deskripsi') is-invalid @enderror" name="deskripsi" autocomplete="deskripsi" {{ ($form_open) ? '' : 'readonly' }}>{{ (old('deskripsi')) ? old('deskripsi') : $pengumumanBup->deskripsi ?? '' }}</textarea>

            @error('deskripsi')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    <div class="form-group row">
        <label for="doc_pengumuman" class="col-md-4 col-form-label text-md-right">{{ __('Dokumen Pengumuman') }}{!! ($pengumumanBup) ? '' : '&nbsp;<span class="text-danger">*</span>' !!}</label>

        <div class="col-md-6">
            @if ($form_open)
            <div class="custom-file">
                <input id="doc_pengumuman" type="file" accept=".pdf" class="{{ ($form_open) ? 'custom-file-input' : 'form-control-plaintext' }} @error('doc_pengumuman') is-invalid @enderror" name="doc_pengumuman" value="{{ (old('doc_pengumuman')) ? old('doc_pengumuman') : $pengumumanBup->doc_pengumuman ?? '' }}" autocomplete="doc_pengumuman" {{ ($pengumumanBup) ? '' : 'required' }} {{ ($form_open) ? 'autofocus' : 'readonly' }}>
                <label class="custom-file-label" for="doc_pengumuman">Choose file</label>
            </div>
            @endif
            @if ($pengumumanBup)
            <div class="mt-2 col-12">
                <a href="{{ route('admin.pengumumanBup.download', $pengumumanBup) }}"><i class="fas fa-download"></i> &nbsp;Pengumuman_{{$pengumumanBup->judul_pengumuman}}.pdf</a>
            </div>
            @endif
            @error('doc_pengumuman')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>
    
    <button type="submit" style="display:none">SUBMIT</button>
</form>