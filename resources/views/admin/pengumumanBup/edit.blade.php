@php
    $title = 'Pengumuman BUP';
    $page = 'Pengumuman BUP';
    $pageTitle = 'Pengumuman BUP';
@endphp

@extends('layouts.dashboard',[
    'title' => $title,
    'page' => $page,
    'pageTitle' => $pageTitle,
    'breadcrumbItems' => [
        [
            'name' => 'Beranda',
            'link' => route('admin.beranda'),
        ],
        [
            'name' => 'Pengumuman BUP',
            'link' => route('admin.pengumumanBup.index'),
        ],
        [
            'name' => $pengumumanBup->judul_pengumuman.' ('.$pengumumanBup->tgl_pameran.')',
            'link' => 'javascript:void(0)',
        ]
    ],
])

@push('action-components')
<div class="me-2">
    <a href="{{ route('admin.pengumumanBup.show', $pengumumanBup) }}" class="btn btn-secondary mr-1">Back</a>
    <button type="button" id="button-submit" class="btn btn-outline-success mr-2">Update</button>
</div>
@endpush

@section('content')
    @include('admin.pengumumanBup.view.form', ['pengumumanBup' => $pengumumanBup, 'form_open' => true])
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('#button-submit').click(function () {
                $('#form-pengumumanBup').find('[type="submit"]').trigger('click');
            });
        });
    </script>
@endpush