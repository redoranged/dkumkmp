<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'prevent-back-history'],function(){
    Route::get('/', function() {
        return redirect()->route('public.beranda');
    });


    /**
     * Route for Public
     */
    Route::get('/public', function() {
        return redirect()->route('public.beranda');
    });

    Route::prefix('public')->name('public.')->group(function() {
        Route::get('/beranda', 'PublicController@beranda')->name('beranda');
        Route::get('/daftarUmkm', 'PublicController@daftarUmkm')->name('daftarUmkm');
        Route::get('/produkUmkm', 'PublicController@produkUmkm')->name('produkUmkm');
        Route::get('/produkUnggulan', 'PublicController@produkUnggulan')->name('produkUnggulan');
        Route::get('/jadwalPameran', 'PublicController@jadwalPameran')->name('jadwalPameran');
        Route::get('/bup', 'PublicController@bup')->name('bup');
        Route::get('/bup/create', 'PublicController@bupCreate')->name('bup.create');
        Route::post('/bup/store', 'PublicController@bupStore')->name('bup.store');
        Route::get('/bup/{pengumumanBup}/download', 'PublicController@bupDownload')->name('pengumumanBup.download');
    });

    Auth::routes();

    /**
     * Route for Admin
     */
    Route::get('/admin', function() {
        return redirect()->route('admin.beranda');
    });

    Route::prefix('admin')->name('admin.')->group(function() {
        Route::get('/beranda', 'AdminController@beranda')->name('beranda');

        // CRUD
        Route::resources([
            'pengguna' => PenggunaController::class,
            'daftarUmkm' => DaftarUmkmController::class,
            'produkUmkm' => ProdukUmkmController::class,
            'produkUnggulan' => ProdukUnggulanController::class,
            'jadwalPameran' => JadwalPameranController::class,
            'bup' => BupController::class,
            'pengumumanBup' => PengumumanBupController::class,
        ]);

        Route::get('/bup/{bup}/download/EKTP', 'BupController@downloadEKTP')->name('bup.download.ektp');
        Route::get('/bup/{bup}/download/KK_Pemilik', 'BupController@downloadKK_Pemilik')->name('bup.download.kk_pemilik');
        Route::get('/bup/{bup}/download/Buku_Tabungan', 'BupController@downloadBuku_Tabungan')->name('bup.download.buku_tabungan');
        Route::get('/bup/{bup}/download/Ket_Usaha', 'BupController@downloadKet_Usaha')->name('bup.download.ket_usaha');
        Route::get('/bup/{bup}/download/Pernyataan', 'BupController@downloadPernyataan')->name('bup.download.doc_pernyataan');

        Route::get('/pengumumanBup/{pengumumanBup}/download', 'PengumumanBupController@download')->name('pengumumanBup.download');
    });
});